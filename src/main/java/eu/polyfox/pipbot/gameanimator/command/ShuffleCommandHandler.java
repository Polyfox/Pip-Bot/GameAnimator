/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.gameanimator.GameAnimatorPlugin;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import eu.polyfox.pipbot.util.EmbedFormatter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'shuffle' sub-command.
 * @author traxam
 */
class ShuffleCommandHandler extends CommandHandler {
    private final GameAnimatorPlugin plugin;

    ShuffleCommandHandler(GameAnimatorPlugin plugin) {
        super(plugin, new String[]{"shuffle", "random", "mix"},
                "En- or disables status shuffling.", "[toggle/on/off]",
                new PermissionRequirement("gameanimator.shuffle", true, false));
        this.plugin = plugin;
    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        if (context.getArguments().length == 0) {
            EmbedBuilder response = context.response("Status shuffling",
                    "*Status shuffling means updating the game status in a random order.*" +
                            "\nShuffle mode is currently **" + (plugin.isShuffleEnabled() ? "enabled" : "disabled") + "**.",
                    EmbedFormatter.INFO)
                    .addField("Commands",
                            "\u25E6 **" + context.getHead() + " toggle** Switches shuffle mode betweeb on and off." +
                                    "\n\u25E6 **" + context.getHead() + " on** Turns shuffle mode on." +
                                    "\n\u25E6 **" + context.getHead() + " off** Turns shuffle mode off.", false);
            context.respond(response.build());
        } else if (context.getArguments()[0].equalsIgnoreCase("toggle")) {
            boolean newShuffleEnabled = !plugin.isShuffleEnabled();
            plugin.setShuffleEnabled(newShuffleEnabled);
            context.respond("Shuffle mode " + (newShuffleEnabled ? "enabled" : "disabled"),
                    "Game status messages will now be displayed in a " +
                            (newShuffleEnabled ? "random" : "linear") + " order. Use `" + context.getHead() + "` to " +
                            "toggle it again.", EmbedFormatter.SUCCESS);
        } else if (context.getArguments()[0].equalsIgnoreCase("on")) {
            if (plugin.isShuffleEnabled())
                context.respond("Shuffle already on", "Shuffle mode is already turned on." +
                        "\n*Use **" + context.getHead() + " off** to turn it off.*", EmbedFormatter.ERROR);
            else {
                plugin.setShuffleEnabled(true);
                context.respond("Turned shuffle mode on", "Shuffle mode was turned on." +
                        "\n*Use **" + context.getHead() + " off** to turn it off again.*", EmbedFormatter.SUCCESS);
            }
        } else if (context.getArguments()[0].equalsIgnoreCase("off")) {
            if (!plugin.isShuffleEnabled())
                context.respond("Shuffle already off", "Shuffle mode is already turned off." +
                        "\n*Use **" + context.getHead() + " on** to turn it on.*", EmbedFormatter.ERROR);
            else {
                plugin.setShuffleEnabled(false);
                context.respond("Turned shuffle mode off", "Shuffle mode was turned off." +
                        "\n*Use **" + context.getHead() + " on** to turn it on again.*", EmbedFormatter.SUCCESS);
            }
        } else
            context.respond("Invalid arguments", "Sorry, `" + context.getArguments()[0] +
                    "` can not be resolved.\n*Use **" + context.getHead() + "** for an overview about status shuffling.",
                    EmbedFormatter.ERROR);
        return null;
    }
}
