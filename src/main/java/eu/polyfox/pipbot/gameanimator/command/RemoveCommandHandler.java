/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.gameanimator.GameAnimatorPlugin;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import eu.polyfox.pipbot.util.EmbedFormatter;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'remove' sub-command.
 * @author tr808axm
 */
class RemoveCommandHandler extends CommandHandler {
    private final GameAnimatorPlugin plugin;

    RemoveCommandHandler(GameAnimatorPlugin plugin) {
        super(plugin, new String[]{"remove", "-"}, "Removes a status from the status list.", "<index>",
                new PermissionRequirement("gameanimator.remove", true, false));
        this.plugin = plugin;
    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        if (context.getArguments().length == 0)
            return createHelpMessage(context);
        else {
            int index;
            try {
                index = Integer.parseInt(context.getArguments()[0]);
                if (index < 0 || index >= plugin.getNumberOfStatuses())
                    throw new IllegalArgumentException();
            } catch (IllegalArgumentException e) {
                context.respond("Invalid argument", "`" + context.getArguments()[0] +
                        "` is not a valid index. The index must be a whole number between `0` and `"
                        + plugin.getNumberOfStatuses() + "`.\n*Please use **" + context.getHead() +
                        " <DEFAULT/LISTENING/WATCHING> <statustext...>**.*", EmbedFormatter.ERROR);
                return null;
            }
            Game removedGame = plugin.removeGame(index);
            context.respond("Status removed", "`[" + removedGame.getType().toString() + "] " +
                    removedGame.getName() + "` was removed from the status list.\n\n:warning: Warning: The removal of " +
                    "this status message is not saved permanently, it might be readded on the next bot startup. For permanent " +
                    "status messages, edit the `status.txt` file located in `plugins/GameAnimator/status.txt`.",
                    EmbedFormatter.SUCCESS);
        }
        return null;
    }
}
