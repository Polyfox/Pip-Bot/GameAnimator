/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.gameanimator.GameAnimatorPlugin;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import eu.polyfox.pipbot.util.EmbedFormatter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'skip' sub-command.
 * @author tr808axm
 */
class SkipCommandHandler extends CommandHandler {
    private final GameAnimatorPlugin plugin;

    SkipCommandHandler(GameAnimatorPlugin plugin) {
        super(plugin, new String[]{"skip", "jump"}, "Skips status messages.", "<to <index>/over <amount>",
                new PermissionRequirement("gameanimator.skip", true, false));
        this.plugin = plugin;
    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        if (context.getArguments().length == 0) {
            EmbedBuilder response = context.response("Status skipping",
                    "*Status skipping means jumping to a specific status or skip a number of statues.*",
                    EmbedFormatter.INFO)
                    .addField("Commands",
                            "\u25E6 **" + context.getHead() + " to <index>** Jumps to a certain status." +
                                    "\n\u25E6 **" + context.getHead() + " over <amount>** Skips an amount of statuses.",
                            false);
            context.respond(response.build());
        } else if (context.getArguments()[0].equalsIgnoreCase("to")) {
            if (context.getArguments().length == 1) // missing index
                context.respond("Missing argument", "Missing the `index` argument.\n*Please use **"
                        + context.getHead() + " to <index>**.*", EmbedFormatter.ERROR);
            else {
                int index;
                try {
                    index = Integer.parseInt(context.getArguments()[1]);
                } catch (NumberFormatException e) {
                    index = -1; // invalid
                }
                if (index < 0 || index >= plugin.getNumberOfStatuses())
                    context.respond("Invalid argument", "`index` must be a whole number between 0 and "
                            + plugin.getNumberOfStatuses() + ".", EmbedFormatter.ERROR);
                else {
                    plugin.skipTo(index);
                    context.respond("Status skipped", "Skipped to status " + index + ".",
                            EmbedFormatter.SUCCESS);
                }
            }
        } else if (context.getArguments()[0].equalsIgnoreCase("over")) {
            if (context.getArguments().length == 1)
                context.respond("Missing argument", "Missing the `amount` argument.\n*Please use **"
                        + context.getHead() + " over <amount>**.*", EmbedFormatter.ERROR);
            else {
                int amount;
                try {
                    amount = Integer.parseInt(context.getArguments()[1]);
                } catch (NumberFormatException e) {
                    amount = 0; // invalid
                }
                if (amount == 0)
                    context.respond("Invalid argument", "`index` must be a whole number other than 0.",
                            EmbedFormatter.ERROR);
                else {
                    plugin.skipOver(amount);
                    context.respond("Status skipped", "Skipped over " + amount + " statuses.",
                            EmbedFormatter.SUCCESS);
                }
            }
        } else
            context.respond("Invalid arguments", "Sorry, `" + context.getArguments()[0] +
                            "` can not be resolved.\n*Use **" + context.getHead() +
                            "** for an overview about status skipping.", EmbedFormatter.ERROR);
        return null;
    }
}
