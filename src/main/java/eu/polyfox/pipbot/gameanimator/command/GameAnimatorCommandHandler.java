/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.gameanimator.GameAnimatorPlugin;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'gameanimator' command.
 * @author tr808axm
 */
public class GameAnimatorCommandHandler extends CommandHandler {
    private final GameAnimatorPlugin plugin;

    public GameAnimatorCommandHandler(GameAnimatorPlugin plugin) {
        super(plugin,
                new String[]{"gameanimator", "status", "statuses"},
                "Pauses or unpauses the game animation.",
                "<pause|unpause>",
                new PermissionRequirement("gameanimator", true, false),
                new ShuffleCommandHandler(plugin),
                new SkipCommandHandler(plugin),
                new ListCommandHandler(plugin),
                new AddCommandHandler(plugin),
                new RemoveCommandHandler(plugin));
        this.plugin = plugin;

    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        if(context.getArguments().length > 0) {
            if(context.getArguments()[0].equalsIgnoreCase("pause")) {
                if(plugin.isPaused())
                    return new MessageBuilder("GameAnimator is already paused.").build();
                else {
                    plugin.setPaused(true);
                    return new MessageBuilder("GameAnimator was paused. Use `" + context.getUsedPrefix()
                            + context.getUsedAlias() + " unpause` to resume the game animation.").build();
                }
            } else if(context.getArguments()[0].equalsIgnoreCase("unpause")) {
                if(plugin.isPaused()) {
                    plugin.setPaused(false);
                    return new MessageBuilder("GameAnimator was unpaused. Use `" + context.getUsedPrefix()
                            + context.getUsedAlias() + " pause` to pause the game animation.").build();
                } else
                    return new MessageBuilder("GameAnimator is already active.").build();
            }
        }
        return new MessageBuilder("Please use `" + context.getUsedPrefix() + context.getUsedAlias() + " <pause|unpause>`.").build();
    }
}
