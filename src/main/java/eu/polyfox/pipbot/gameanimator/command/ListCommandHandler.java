/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.gameanimator.GameAnimatorPlugin;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import eu.polyfox.pipbot.util.EmbedFormatter;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'list' sub-command.
 * @author tr808axm
 */
class ListCommandHandler extends CommandHandler {
    private final GameAnimatorPlugin plugin;

    ListCommandHandler(GameAnimatorPlugin plugin) {
        super(plugin, new String[]{"list", "list-statuses"}, "Provides an indexed list of status messages.",
                "", new PermissionRequirement("gameanimator.list", true, false));
        this.plugin = plugin;
    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        StringBuilder statusList = new StringBuilder("The following is a list of all game status messages with the index, type ");
        for (int i = 0; i < plugin.getGameList().size(); i++)
            statusList.append("\n\u25E6 `").append(i).append("`: `").append(plugin.getGameList().get(i).getType().toString())
                    .append("` ").append(plugin.getGameList().get(i).getName());
        context.respond("Status list", statusList.toString(), EmbedFormatter.INFO);
        return null;
    }
}
