/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator.command;

import eu.polyfox.pipbot.command.CommandHandler;
import eu.polyfox.pipbot.command.CommandInvocationContext;
import eu.polyfox.pipbot.gameanimator.GameAnimatorPlugin;
import eu.polyfox.pipbot.permission.PermissionRequirement;
import eu.polyfox.pipbot.util.EmbedFormatter;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Message;

/**
 * Handles the 'add' sub-command.
 * @author tr808axm
 */
class AddCommandHandler extends CommandHandler {
    private final GameAnimatorPlugin plugin;

    AddCommandHandler(GameAnimatorPlugin plugin) {
        super(plugin, new String[]{"add", "+"}, "Adds a status to the status list.", "<DEFAULT/LISTENING/WATCHING> <statustext...>",
                new PermissionRequirement("gameanimator.add", true, false));
        this.plugin = plugin;
    }

    @Override
    protected Message execute(CommandInvocationContext context) {
        if (context.getArguments().length == 0)
            return createHelpMessage(context);
        else {
            Game.GameType gameType;
            try {
                gameType = Game.GameType.valueOf(context.getArguments()[0]);
            } catch (IllegalArgumentException e) {
                context.respond("Invalid argument", "`" + context.getArguments()[0] +
                        "` is not a valid game type. You can use `DEFAULT`, `LISTENING` or `WATCHING`.\n*Please use **"
                        + context.getHead() + " <DEFAULT/LISTENING/WATCHING> <statustext...>**.*", EmbedFormatter.ERROR);
                return null;
            }

            if (context.getArguments().length == 1)
                context.respond("Missing argument", "Missing the `statustext` argument.\n*Please use **" +
                        context.getHead() + ' ' + context.getArguments()[0] + " <statustext...>**.*", EmbedFormatter.ERROR);
            else {
                StringBuilder statusTextBuilder = new StringBuilder(context.getArguments()[1]);
                for (int i = 2; i < context.getArguments().length; i++)
                    statusTextBuilder.append(' ').append(context.getArguments()[i]);
                String statusText = statusTextBuilder.toString();
                int index = plugin.addGame(Game.of(gameType, statusText));
                context.respond("Status added", "`[" + gameType.toString() + "] " + statusText +
                        "` was added to the status list with index `" + index +
                        "`.\n\n:warning: Warning: This new status message is not saved permanently, " +
                        "it will be removed on the next bot startup. For permanent status messages, edit the `status.txt` " +
                        "file located in `plugins/GameAnimator/status.txt`.", EmbedFormatter.SUCCESS);
            }
        }
        return null;
    }
}
