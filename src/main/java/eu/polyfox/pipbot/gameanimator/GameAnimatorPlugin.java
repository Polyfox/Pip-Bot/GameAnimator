/*
 * MIT License
 *
 * Copyright (c) 2018 traxam
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package eu.polyfox.pipbot.gameanimator;

import eu.polyfox.pipbot.gameanimator.command.GameAnimatorCommandHandler;
import eu.polyfox.pipbot.plugin.Plugin;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.core.entities.Game;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

/**
 * Plugin for Pip-Boy that regularly changes the game status.
 * @author tr808axm
 */
@Slf4j
public class GameAnimatorPlugin extends Plugin {
    private Timer timer;

    private List<Game> gameList;
    private int currentGameIndex = 0;
    private TimerTask gameAnimatorTask;
    @Getter @Setter private boolean shuffleEnabled = false;

    @Override
    public void enable() {
        // ensure plugin folder exists and abort if it could not be created
        if (!getMetadata().getPluginFolder().exists() && !getMetadata().getPluginFolder().mkdirs())
            throw new RuntimeException("Plugin folder " + getMetadata().getPluginFolder().getAbsolutePath() +
                    " could not be created.");

        // load (and create if necessary) the status config file
        File statusFile = new File(getMetadata().getPluginFolder(), "status.txt");
        if (!statusFile.exists()) {
            try {
                Files.copy(getClass().getResourceAsStream("status.txt"), statusFile.toPath());
            } catch (IOException e) {
                throw new RuntimeException("Could not create status configuration file", e);
            }
        }

        // read the config file
        try {
            List<String> contents = Files.readAllLines(statusFile.toPath());
            gameList = contents.stream()
                    .filter(s -> !s.isEmpty() && s.charAt(0) != '#') // ignore empty entries and entries starting with #
                    .map(s -> Game.of( // parse Game object from every line
                            Game.GameType.valueOf(s.substring(0, s.indexOf(' '))), // game type before first space
                            s.substring(s.indexOf(' ') + 1)
                    )).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException("Could not read status configuration file", e);
        }



        getPipBot().getCommandManager()
                .registerCommandHandler(new GameAnimatorCommandHandler(this));
        timer = new Timer("GameAnimator");
        setPaused(false);
        log.info("Enabled GameAnimator.");
    }

    @Override
    public void disable() {
        setPaused(true);
        timer.cancel();
        log.info("Disabled GameAnimator.");
    }

    @Override
    public String getName() {
        return "GameAnimator";
    }

    @Override
    public String getAuthorName() {
        return "traxam#7012";
    }

    @Override
    public long getAuthorId() {
        return 137263174675070976L;
    }

    @Override
    public String getWebsite() {
        return "https://gitlab.com/Polyfox/Pip-Bot/GameAnimator";
    }

    /**
     * @return whether the animation is paused (not active).
     */
    public boolean isPaused() {
        return gameAnimatorTask == null;
    }

    /**
     * Pauses / unpauses the game animation.
     * @param newPaused whether the animation should be paused.
     */
    public void setPaused(boolean newPaused) {
        if (newPaused) {
            if(gameAnimatorTask != null)
                gameAnimatorTask.cancel();
            gameAnimatorTask = null;
            log.info("Game animation was paused.");
        } else {
            timer.scheduleAtFixedRate(gameAnimatorTask = new TimerTask() {
                @Override
                public void run() {
                    skipOver(1); // move to next one
                }
            }, 0, 60000);
            log.info("Game animation was restarted.");
        }
    }

    public int getNumberOfStatuses() {
        return gameList.size();
    }

    public void skipTo(int index) {
        currentGameIndex = index;
        updatePresence();
    }

    public void skipOver(int amount) {
        if (shuffleEnabled)
            currentGameIndex = (int) (Math.random() * gameList.size());
        else {
            currentGameIndex += amount;
            currentGameIndex %= gameList.size();
        }
        updatePresence();
    }

    /**
     * Updates the actual game text displayed in Discord.
     */
    private void updatePresence() {
        Game game = gameList.size() == 0 ? null : gameList.get(currentGameIndex);
        getPipBot().getJda().getPresence().setGame(game);
    }

    public List<Game> getGameList() {
        return gameList;
    }

    /**
     * @return the index of the added game.
     */
    public int addGame(Game game) {
        gameList.add(game);
        return gameList.size() - 1;
    }

    public Game removeGame(int index) {
        return gameList.remove(index);
    }
}
