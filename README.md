# GameAnimator
Game-status management plugin for your Discord [Pip-Bot](https://gitlab.com/Polyfox/Pip-Bot/Pip-Bot).

## Features
- Automated game status changes in random or linear order
- Configurable game statuses in the `plugins/GameAnimator/status.txt` config file or using commands
- Pausing and skipping game statuses

## Download
Download the latest version on the [tags page](https://gitlab.com/Polyfox/Pip-Bot/GameAnimator/tags).

## Commands
All commands must be executed with the prefix of your bot. GameAnimator commands can only be executed by the bot host(s).

### `<prefix>gameanimator`
Base command, all other commands are derivative to this command. Provides an in-chat overview over all commands of this 
plugin.

### `<prefix>gameanimator pause`
Pauses the game status animation cycle. Use `<prefix>gameanimator unpause` to unpause the game animation cycle again.

### `<prefix>gamanimator unpause`
Unpauses the game status animation cycle and immediately skips to the next game status. Use 
`<prefix>gameanimator pause` to pause the animation cycle again.

### `<prefix>gameanimator shuffle <toggle/on/off>`
Change whether shuffle mode is enabled. Use `toggle` to switch between the `on` and `off` state or use `on` or `off` to
switch it on or off directly. If shuffle mode is enabled, game statuses are displayed in a random order (they are 
displayed in a linear order otherwise).

### `<prefix>gameanimator skip to <index>`
Makes the status queue jump to the specified index. Use `<prefix>gameanimator list` to 
find out which game status has which index.

### `<prefix>gameanimator skip over <amount>`
Skips `<amount>` game statuses in the status queue.

### `<prefix>gameanimator list`
Lists the whole status queue with indexes, game types and status contents.

### `<prefix>gameanimator add <DEFAULT/LISTENING/WATCHING> <status-text...>`
Adds a game status to the end of the status queue. Use `DEFAULT` for a `Playing ...` game status, `LISTENING` for
`Listening to ...` and `WATCHING` for a `Watching ...` one. (`STREAMING` does not work as you can not provide a twitch 
link)

*Warning:* Game statuses added with this command are deleted when the bot is stopped. For permanent game status changes, 
edit the `status.txt` file located in `plugins/GameAnimator/status.txt`.

### `<prefix>gameanimator remove <index>`
Removes the game status with the specified index from the status queue. Use `<prefix>gameanimator list` to find out which 
game status has which index.

*Warning:* Game statuses removed with this command are restored when the bot is stopped. For permanent game status changes,
edit the `status.txt` file located in `plugins/GameAnimator/status.txt`.